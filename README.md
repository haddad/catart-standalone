The concatenative real-time sound synthesis system cataRT plays grains (also called units) from a large corpus of segmented and descriptor-analysed sounds according to proximity to a target position in the descriptor space. This can be seen as a content-based extension to granular synthesis providing direct access to specific sound characteristics.

The standalone application has an easy-to-use interface, extensive documentation, complete MIDI and keyboard control, and a preset system allowing interpolation of presets via a MIDI controller.

![](https://forum.ircam.fr/media/uploads/Softwares/catart-0.9.9.png)

## Fields of Application ##
**Universal**

- analysis, visualization, and sound interaction
- analysis data export

**Pedagogical**

- graphic representation of a sound and perceptual descriptors
- interactive analysis of the temporal structure of a sound

**Composition**

- all parameters for the description and transformation of sound are available
- composition via interactive navigation in a descriptor space
- MIDI compatibility and communication

**Cinema, Video, and Post-Production**

- re-synthesis, ambience control and “soundscape” textures
- specific event or sonic feature search in long recordings
- noise separation

**Musicology**

- perceptual analysis and annotation
- Sound Design
- interactive navigation in a sound descriptor space
- high level control via sound descriptors
- mixing and juxtaposition of massive amounts of sound samples
- flexibility of granular synthesis

## Format ##

C++ Library, MacOSX, Windows

## Design and Development ##

CataRT Standalone is developed by the [Sound Music Movement Interaction team](https://www.ircam.fr/recherche/equipes-recherche/ismm/) at IRCAM.

## Installation Guide ##

CataRT Standalone is a collective build that needs Max runtime installed.
To install Max runtime, simply download Max.
You don't need to buy a Max licence because Catart standalone will run in Max demo version.
